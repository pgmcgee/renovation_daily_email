class CreateDays < ActiveRecord::Migration
  def change
    create_table :days do |t|
      t.date :date
      t.string :day
      t.string :scripture
      t.text :scripture_html
      t.text :notes_html
      t.integer :week_id

      t.timestamps
    end
  end
end
