class DaySerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :date, :day, :scripture, :scripture_html, :notes_html, :week_id
end
