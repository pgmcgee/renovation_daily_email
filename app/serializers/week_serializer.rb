class WeekSerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :start_date

  has_many :days
end
