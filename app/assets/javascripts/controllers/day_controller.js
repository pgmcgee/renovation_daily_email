RenovationDailyEmail.DayController = Ember.ObjectController.extend(RenovationDailyEmail.AutosavableController, {
  bufferedFields: ['scriptureHtml', 'notesHtml'],

  actions: {
    setScripture: function (scripture) {
      if (scripture === "") { return; }
      var that = this;
      $.get('/scripture/query?q=' + scripture, {}, function (data) {
        that.get('model').set('scripture', data.scripture);
        that.get('model').set('scriptureHtml', data.body);

        NProgress.set(0).start();
        that.get('model').on('didUpdate', function() {
          NProgress.done();
        });
        that.get('model').save();
      });
    }
  }
});