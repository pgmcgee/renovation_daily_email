RenovationDailyEmail.IndexController = Ember.ArrayController.extend({
  itemController: 'week',
  actions: {
    changeWeek: function(week) {
      this.set('selectedWeek', week);
    }
  }
});