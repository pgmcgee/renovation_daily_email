RenovationDailyEmail.WeekController = Ember.ObjectController.extend({
  isActive: function() {
    return this.parentController.get('selectedWeek') == this.get('model');
  }.property('parentController.selectedWeek', 'model')
});