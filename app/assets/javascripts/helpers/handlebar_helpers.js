Ember.Handlebars.registerBoundHelper('format-date', function (value, options) {
  var date = Handlebars.Utils.escapeExpression(value);
  var formatted = moment(date).utc().format(options.hash.format);

  // Some more optional formatting
  if (options.hash.format === 'MMM') formatted = formatted.toUpperCase();

  return new Handlebars.SafeString(formatted);
});