RenovationDailyEmail.DaysRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('week', params.id);
    },
    setupController: function(controller, week) {
        controller.set('model', week.get('days'));
    }
});