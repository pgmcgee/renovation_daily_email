RenovationDailyEmail.IndexRoute = Ember.Route.extend({
  model: function (params) {
    return this.store.findAll('week');
  },
  setupController: function (controller, weeks) {
    controller.set('model', weeks);
    controller.set('selectedWeek', weeks.objectAt(1));
  }
});
