// for more details see: http://emberjs.com/guides/models/defining-models/

RenovationDailyEmail.Week = DS.Model.extend({
  startDate: DS.attr('date'),
  days: DS.hasMany('day')
});
