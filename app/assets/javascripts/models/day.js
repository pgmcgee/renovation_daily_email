// for more details see: http://emberjs.com/guides/models/defining-models/

RenovationDailyEmail.Day = DS.Model.extend(RenovationDailyEmail.AutosavableModel, {
  date: DS.attr('date'),
  day: DS.attr('string'),
  scripture: DS.attr('string'),
  scriptureHtml: DS.attr('string'),
  notesHtml: DS.attr('string'),
  week: DS.belongsTo('week')
});
