// for more details see: http://emberjs.com/guides/views/

RenovationDailyEmail.DayView = Ember.View.extend({
  didInsertElement : function(){
    var that = this;
    Ember.run.schedule('afterRender',function(){
      that.$('.ckeditor').ckeditor({
        toolbar: [
          { name: 'document', items: [ 'Source' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
          [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
          { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
          { name: 'paragraph', items: [ 'NumberedList', 'BulletedList' ] },
          { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ]
      })
      var editor = that.$('.ckeditor').ckeditorGet();
      editor.setData(that.get('controller').get('notesHtml'));
      editor.on('change', function(e) {
        if (e.editor.checkDirty()) {
          that.get('controller').set('notesHtml', this.getData());
        }
      });
    });
  }
});
