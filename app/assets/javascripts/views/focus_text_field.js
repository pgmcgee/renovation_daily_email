RenovationDailyEmail.FocusTextField = Ember.TextField.extend({
    focusOut: function() {
        this.sendAction('action', this.get('value'));
    }
});
Ember.Handlebars.helper('focus-text-field', RenovationDailyEmail.FocusTextField);