atom_feed do |feed|
  feed.title "Walk It Out Feed"
  feed.updated(@days[0].date) if @days.length > 0

  @days.each do |day|
    feed.entry(day, published: day.date, updated: day.date) do |entry|
      entry.title day.date.strftime("%B %e, %Y")
      entry.content day.body, type: 'html'
    end
  end
end