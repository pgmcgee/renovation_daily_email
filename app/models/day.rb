require 'nokogiri'
require 'scripture_parser'

class Day < ActiveRecord::Base
  belongs_to :week

  def body
    #scripture_html + notes_html
    scripture_doc = Nokogiri::HTML(scripture_html)
    scripture_body = scripture_doc.css('body').inner_html
    new_notes_html = ScriptureParser.replace(notes_html, '<a href="http://bible.com/59/\1.esv">\1</a>')

    scripture_body + "<h3 class='notes'>Notes on #{scripture}</h3>" + new_notes_html
  end
end
