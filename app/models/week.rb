class Week < ActiveRecord::Base
  has_many :days, -> { order 'date ASC' }

  after_create :create_days

  private

  def create_days
    days_of_week = %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday)
    current_date = start_date

    7.times do
      days.create(
        {
          date: current_date,
          day: days_of_week[current_date.wday],
          scripture: "",
          scripture_html: "",
          notes_html: ""
        }
      )
      current_date = current_date.advance({days: 1})
    end
  end
end
