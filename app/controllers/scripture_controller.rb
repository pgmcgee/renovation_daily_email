require 'net/http'
require 'nokogiri'

class ScriptureController < ApplicationController
  def query
    uri = URI('http://www.esvapi.org/v2/rest/passageQuery')
    uri_params = {
        key: 'IP',
        passage: params[:q],
        include_footnotes: 'false',
        include_headings: 'false',
        include_audio_link: 'false',
        include_short_copyright: 'false',
    }
    uri.query = URI.encode_www_form(uri_params)
    results = Net::HTTP.get(uri)

    if results.include? "ERROR"
      render nothing: true, status: 404
    else
      doc = Nokogiri::HTML(results)
      doc.css('.chapter-num').remove

      json_result = {
        scripture: doc.css('h2').inner_text,
        body: doc.to_s
      }
      render json: json_result
    end
  end
end
