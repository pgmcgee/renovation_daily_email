class WeeksController < ApplicationController
  def index
    current_sunday = (Date.today + 1.day).sunday
    next_sunday = current_sunday.advance(weeks: 1)
    previous_sunday = current_sunday.advance(weeks: -1)

    @weeks = [
      Week.where(start_date: previous_sunday).first_or_create,
      Week.where(start_date: current_sunday).first_or_create,
      Week.where(start_date: next_sunday).first_or_create,
    ]

    render json: @weeks
  end
end
