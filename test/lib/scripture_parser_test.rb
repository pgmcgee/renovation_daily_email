require 'test_helper'
require 'scripture_parser'

class ScriptureParserTest < ActionView::TestCase
  test 'parse single scripture, chapter only' do
    assert_equal ['1 Corinthians 5'], ScriptureParser.parse('1 Corinthians 5')
  end

  test 'parse single scripture' do
    assert_equal ['1 Corinthians 5:8'], ScriptureParser.parse('1 Corinthians 5:8')
  end

  test 'parse multiple scriptures' do
    assert_equal ['1 Corinthians 5:8', 'Mark 5:1'], ScriptureParser.parse('1 Corinthians 5:8 and Mark 5:1')
  end

  test 'parse scripture with multiple verses' do
    assert_equal ['1 Corinthians 5:8-10'], ScriptureParser.parse('1 Corinthians 5:8-10')
  end

  test 'parse multiple scriptures with multiple verses' do
    assert_equal ['1 Corinthians 5:8-10', 'Mark 5:1-4'], ScriptureParser.parse('1 Corinthians 5:8-10, and Mark 5:1-4')
  end

  test 'parse many varieties of scriptures' do
    assert_equal ['Ezekiel 7', 'Matthew 28:6', '1 Corinthians 5:8-10', 'Mark 5:1-4'], ScriptureParser.parse('Ezekiel 7, Matthew 28:6, 1 Corinthians 5:8-10, and Mark 5:1-4')
  end

  test 'replace many varieties of scriptures' do
    assert_equal '<span>Ezekiel 7</span>, <span>Matthew 28:6</span>, <span>1 Corinthians 5:8-10</span>, and <span>Mark 5:1-4</span>',
                 ScriptureParser.replace('Ezekiel 7, Matthew 28:6, 1 Corinthians 5:8-10, and Mark 5:1-4', '<span>\1</span>')
  end
end
