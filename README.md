# Renovation Daily Email

A frontend to a database to manage daily scripture readings and notes for those readings. It exposes an Atom endpoint (/days.atom) that Mailchimp can read to send out daily emails. This was primarily a project to learn Ember.js, but it has been used daily for months.

Tests need to be added.
